variable "image_flavor" {
  type = string
  default = "Ubuntu-22.04-202208"
}

variable "compute_flavor" {
  type = string
  default = "STD2-1-1"
}

variable "key_pair_name" {
  type = string 
  default = "id_rsa"
}

variable "availability_zone_name" {
  type = string
  default = "GZ1"
}



